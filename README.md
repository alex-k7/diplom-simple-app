## Сборка образа тестового приложения и его деплой в кластер Kubernetes.
Манифест Kubernetes для деплоя приложения  - [nginx-app.yaml
](https://gitlab.com/alex-k7/diplom-simple-app/-/blob/main/kubernetes/nginx-app.yaml).

[TEST APP](http://51.250.27.127/)

 [Docker image](https://gitlab.com/alex-k7/diplom-simple-app/container_registry/3152555).

 [Build pipeline](https://gitlab.com/alex-k7/diplom-simple-app/-/pipelines/577985860).

 [Deploy pipeline](https://gitlab.com/alex-k7/diplom-simple-app/-/pipelines/577988664).